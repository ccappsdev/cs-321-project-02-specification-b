
/***
 * 
 * Jester Class: Outputs messages for the Start, Middle and End of each Match
 * 
 */
public class Jester {
	
	/***
	 *  Jester Constructor - No functionality
	 */
	public Jester() {

	}
	
	/***
	 * <p>Comment at the beginning of the Match, print which Fighter
	 * has the advantage.</p>
	 * 
	 * @param fighter1 Fighter in the Match (1 of 2)
	 * @param fighter2 Fighter in the Match (2 of 2)
	 */
	public void CommentOnStart(Fighter fighter1, Fighter fighter2) {
		
		Fighter adv = null; //Fighter with advantage
		
		//If fighter1 has at least two physical attributes higher than fighter2
    	if(fighter1.FasterThan(fighter2) && fighter1.LongerReachedThan(fighter2)
    			|| fighter1.FasterThan(fighter2) && fighter1.StrongerThan(fighter2)
    			|| fighter1.LongerReachedThan(fighter2) && fighter1.StrongerThan(fighter2))
    	{
    		adv = fighter1;
    	}
    	else
    	{
    		adv = fighter2;
    	}
    	
		String comment = "\nAlright! " + fighter1.getName() + " and "
				+ fighter2.getName() + " are about to face off! Looks like "
				+ adv.getName() + " has the advantage!";
		
		System.out.println(comment);

	}

	/***
	 * <p>Comment when a Fighter in the match is at half-health</p>
	 * 
	 * @param fighter Fighter with half-health in the Match
	 */
	public void CommentOnMiddle(Fighter fighter) {
		
			String comment = "Well, looks like " + fighter.getName() + " is about"
					+ " halfway to the grave. Remember, it's too late to cancel your bets!";
			
			System.out.println(comment);

	}
	
	/***
	 * <p>Comment of the end of the Match and print the winner and loser</p>
	 * 
	 * @param winner Fighter - Winner of the Match
	 * @param loser Fighter - Loser of the Match
	 */
	public void CommentOnEnd(Fighter winner, Fighter loser) {
		
		
			Fighter adv = null; //Fighter with advantage
			
			//If fighter1 has at least two physical attributes higher than fighter2
	    	if(winner.FasterThan(loser) && winner.LongerReachedThan(loser)
	    		|| winner.FasterThan(loser) && winner.StrongerThan(loser)
	     		|| winner.LongerReachedThan(loser) && winner.StrongerThan(loser))
	    	{
	    		adv = winner;
	    	}
	    	else
	    	{
	    		adv = loser;
	    	}
	    	
	    	String comment = "\nIt's over! " + loser.getName() + " has lost in spectacular fashion!";
	    	
	    	if (adv.equals(loser))
				comment += " Never thought " + loser.getName() + " would bungle an advantage like that!";
				
				System.out.println(comment + "\n");

	}

}
