import java.util.ArrayList;

/**
 *  SubTournament Class: Handles playing through the matches for a given SubTournament of a chosen type
 * 
 */
public class SubTournament {

    public ArrayList<Match> matches;
    public TournamentArchetype archetype;

    /**
     *  <p>SubTournament Constructor - Creates the initial matchups 
     *  given a list of fighters and SubTournament type</p>
     * 
     *  @param archetype TournamenArchetype - Determines the type of SubTournament [SHORT, MEDIUM, LONG, WILD]
     *  @param numMatches int - Number of starting matches played in SubTournament
     *  @param fighters ArrayList<Fighter> - List of fighters to participate in the SubTournament 
     */
    public SubTournament(TournamentArchetype archetype, int numMatches, ArrayList<Fighter> fighters) {

        this.archetype = archetype;
        this.matches = new ArrayList<Match>();
        
        int currFighter = 0;
        for (int i = 0; i < numMatches; i++) {
            Match match = new Match();
            match.setFighters(fighters.get(currFighter++), fighters.get(currFighter++));
            this.matches.add(match);
        }

    }

    /***
     * <p>Plays through the list of inital matches for the SubTournament,
     * then recursively progresses through each round until a single Fighter
     * remains in the matchWinners list, this is the winner of the SubTournament.</p>
     * 
     * @param roundNum int - The current round in the SubTournament bracket; for console output purposes.
     * @return matchWinner.get(0) - The final winner of the SubTournament (recursively defined)
     */
    public Fighter DetermineWinner(int roundNum) {
        
        int matchNum = 1;
        ArrayList<Fighter> matchWinners = new ArrayList<Fighter>();
       
        for (Match match : this.matches) {
            System.out.println("#=========================================");
            System.out.println(this.archetype.name() + " SUBTOURNAMENT - ROUND: " + roundNum + " MATCH: " + matchNum);
            System.out.println("#=========================================");
            matchWinners.add(match.PlayMatch());
            matchNum++;
        }

        if (matchWinners.size() > 1) {
            SubTournament nextRound = new SubTournament(this.archetype, matchWinners.size() / 2, matchWinners);
            roundNum++;
            return nextRound.DetermineWinner(roundNum);
        }

        return matchWinners.get(0);
    }

}

