import java.util.*;
import java.util.Random;

/***
 * WeaponFactory Class - Factory class for creating different types of Weapon Objects
 */
public class WeaponFactory {

    private Map<String, Integer> atkRatings= new HashMap<String, Integer>();
    private Map<String, Integer> defRatings= new HashMap<String, Integer>();

    /***
     * <p>WeaponFactory Constructor - Adds the Attack and Defense Ratings for
     * each possible weapon into the respective HashMaps</p>
     */
    public WeaponFactory() {

        // SHORT Weapons
        this.atkRatings.put("Dagger", new Integer(4));
        this.atkRatings.put("Cestus", new Integer(5));
        this.atkRatings.put("Gladius", new Integer(3));
        
        this.defRatings.put("Dagger", new Integer(1));
        this.defRatings.put("Cestus", new Integer(0));
        this.defRatings.put("Gladius", new Integer(3));

         // MEDIUM Weapons
        this.atkRatings.put("Staff", new Integer(1));
        this.atkRatings.put("Hand-and-a-half sword", new Integer(3));
        this.atkRatings.put("Rapier", new Integer(3));

        this.defRatings.put("Staff", new Integer(3));
        this.defRatings.put("Hand-and-a-half sword", new Integer(2));
        this.defRatings.put("Rapier", new Integer(1));

        // LONG Weapons
        this.atkRatings.put("Halberd", new Integer(3));
        this.atkRatings.put("Lance", new Integer(1));
        this.atkRatings.put("Two-handed sword", new Integer(2));

        this.defRatings.put("Halberd", new Integer(0));
        this.defRatings.put("Lance", new Integer(2));
        this.defRatings.put("Two-handed sword", new Integer(2));

    }

    /***
     * <p>Creates a random weapon of the given archetype.
     * Weapon Name, and Attack/Defense ratings are pulled from the HashMaps</p>
     * 
     * @param archetype WeaponArchetype - Type of weapon being created [SHORT, MEDIUM, LONG]
     * @return weapon - Weapon object of the given archetype created by the method.
     */
    public Weapon MakeWeapon(WeaponArchetype archetype) {
        
        if(archetype == null) {
            Random random = new Random();
            WeaponArchetype randomWeaponType = WeaponArchetype.values()[random.nextInt(WeaponArchetype.values().length)];
            archetype = randomWeaponType;
        }

        if(archetype == WeaponArchetype.SHORT) {
            
            Random random = new Random();
            Weapon weapon = new Weapon();
            weapon.setArchetype(archetype);

            int weaponChoice = random.nextInt(3) + 1;
            switch(weaponChoice) {
                case 1:
                    weapon.attackRating = this.atkRatings.get("Dagger");
                    weapon.defenseRating = this.defRatings.get("Dagger");
                    break;
                case 2:
                    weapon.attackRating = this.atkRatings.get("Cestus");
                    weapon.defenseRating = this.defRatings.get("Cestus");
                    break;
                case 3:
                    weapon.attackRating = this.atkRatings.get("Gladius");
                    weapon.defenseRating = this.defRatings.get("Gladius");
                    break;
                default:
                    weapon.attackRating = this.atkRatings.get("Dagger");
                    weapon.defenseRating = this.defRatings.get("Dagger");
            }

            return weapon;
        } 
        else if(archetype == WeaponArchetype.MEDIUM) {

            Random random = new Random();
            Weapon weapon = new Weapon();
            weapon.setArchetype(archetype);

            int weaponChoice = random.nextInt(3) + 1;
            switch(weaponChoice){
                case 1:
                    weapon.attackRating = this.atkRatings.get("Staff");
                    weapon.defenseRating = this.defRatings.get("Staff");
                    break;
                case 2:
                    weapon.attackRating = this.atkRatings.get("Hand-and-a-half sword");
                    weapon.defenseRating = this.defRatings.get("Hand-and-a-half sword");
                    break;
                case 3:
                    weapon.attackRating = this.atkRatings.get("Rapier");
                    weapon.defenseRating = this.defRatings.get("Rapier"); 
                    break;
                default:
                    weapon.attackRating = this.atkRatings.get("Staff");
                    weapon.defenseRating = this.defRatings.get("Staff");
            }
            return weapon;
        } 
        else if(archetype == WeaponArchetype.LONG) {

            Random random = new Random();
            Weapon weapon = new Weapon();
            weapon.setArchetype(archetype);
            
            int weaponChoice = random.nextInt(3) + 1;
            switch(weaponChoice){
                case 1:
                    weapon.attackRating = this.atkRatings.get("Halberd");
                    weapon.defenseRating = this.defRatings.get("Halberd");
                    break;
                case 2:
                    weapon.attackRating = this.atkRatings.get("Lance");
                    weapon.defenseRating = this.defRatings.get("Lance");
                    break;
                case 3:
                    weapon.attackRating = this.atkRatings.get("Two-handed sword");
                    weapon.defenseRating = this.defRatings.get("Two-handed sword"); 
                    break;
                default:
                    weapon.attackRating = this.atkRatings.get("Halberd");
                    weapon.defenseRating = this.defRatings.get("Halberd");
            }
            return weapon;
        }
        else {
            return null;
        }
   }
}
