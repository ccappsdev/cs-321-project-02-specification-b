/***
 * TournamentArchetype Enumeration: Possible types of SubTournaments played in the main Tournament.
 */
public enum TournamentArchetype {
    LONG,
    MEDIUM,
    SHORT,
    WILD
}
