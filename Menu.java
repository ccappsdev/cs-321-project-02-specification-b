import java.util.*;

/**
 *  Menu Class: Prompts the user for input to start Tounament.
 *  Runs through Tournament to delare winner.
 */
class Menu {
    
    /**
     *  Menu Contstructor: No functionality
     */
    public Menu() {
    }

    /***
     *  <p>Asks the user to input the number of matches per
     *  SubTournament, then starts the corresponding Tournament.
     *  Then plays through Tounamenr to determine winner.</p>
     *  Tournament must contain 2^n number of matches per SubTournament.
     */
    public void RunTournament() 
    {
    	Scanner input = new Scanner(System.in);
    	System.out.println("Please input the number of matches : ");
    	int userInput = input.nextInt();
    	int numMatches = userInput;
        
        // if number of matches is power of 2, start Tournament
        if (numMatches != 0 && (numMatches & (numMatches - 1)) == 0) {
            Tournament theTournament = new Tournament(numMatches);
            Fighter theWinner = theTournament.DetermineWinner();

            System.out.println("\n=============================");
            System.out.println("TOUNAMENT WINNER: " + theWinner.getName());
            System.out.println("=============================\n");
   
            System.out.println("That's it folks! Show's over! " + theWinner.getName() + " won! Good Night!\n\n");

        }
        else {
            System.out.println("Please enter a number that is a power-of-two: ");
            RunTournament();
        }
        input.close();

    }

}
