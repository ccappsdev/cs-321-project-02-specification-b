import java.util.*;

/***
 * Fighter Class: Holds stats for a Figher in the Tournament
 */
public class Fighter 
{
	
	public int strength, reach, speed;
	public Weapon weapon;
	public String Name;
	
	public Fighter(){
			
	}

	public void setName(String Name) {
        this.Name = Name;
    }

    public void setReach(int reach) {
        this.reach = reach;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setWeapon(Weapon weapon){
        this.weapon = weapon;
	}
	
	public String getName() {
        return this.Name;
    }

    public int getReach() {
        return this.reach;
    }

    public int getStrength() {
        return this.strength;
    }

    public int getSpeed() {
        return this.speed;
    }

    public Weapon getWeapon(){
        return this.weapon;
    }

	/***
	 * <p>Determine which Fighter in the Match is Stronger</p>
	 * 
	 * @param opponent Fighter being compared to this.Fighter
	 * @return Boolean - True: this.Fighter is Stronger False: opponent is Stronger
	 */
    public boolean StrongerThan(Fighter opponent) {

		if (this.strength > opponent.getStrength()) {
			return true;
		}
		
		return false;
	}
	
	/***
	 * <p>Determine which Fighter in the Match is Longer Reached</p>
	 * 
	 * @param opponent Fighter being compared to this.Fighter
	 * @return Boolean - True: this.Fighter is Longer Reached False: opponent is Longer Reached
	 */
	public boolean LongerReachedThan(Fighter opponent) {

		if (this.reach > opponent.getReach()) {
			return true;
		}
		
		return false;
	}
	
	/***
	 * <p>Determine which Fighter in the Match is Faster</p>
	 * 
	 * @param opponent Fighter being compared to this.Fighter
	 * @return Boolean - True: this.Fighter is Faster False: opponent is Faster
	 */
	public boolean FasterThan(Fighter opponent) {

		if (this.speed > opponent.getSpeed()) {
			return true;
		}

		return false;
	}

	/***
	 * <p>Roll a six-sided die for each point in the Fighter's
	 * Weapon.attackRating. Dice rolls accumulate for total
	 * damage produced this combat round</p>
	 * 
	 * @return int - The total damage produced this round by the fighter
	 */
	public int GetAttackPerformance() {
		
		Random rand = new Random();
		int damage = 0;
	
		for (int i = 0; i < weapon.getAttackRating(); i++) { 
			damage += rand.nextInt(6) + 1;
		}
		
		return damage;
	}
	
	/***
	 * <p>Roll a six-sided die for each point in the Fighter's
	 * Weapon.defenseRating. Dice rolls accumulate for total
	 * defense produced this combat round</p>
	 * 
	 * @return int - The total defense produced this round by the fighter
	 */
	public int GetDefensePerformance() {
		Random rand = new Random();
		int defense = 0;
		
		for (int i = 0; i < weapon.getDefenseRating(); i++) {
			defense += rand.nextInt(6) + 1;
		}
		
		return defense;
	}

}
