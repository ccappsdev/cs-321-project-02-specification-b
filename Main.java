
class Main {

	/**
     *  <p>Main method; program entry point
     *  Creates a new Menu Object</p>
     *  @param args[] Array of String Objects used for CL I/O operations. 
     */
    public static void main(String[] args) {
        System.out.println("Welcome to the Medieval Combat Tournament!");
        
        //Create main Menu Object the user can start the tournament with.
        Menu mainMenu = new Menu();
        mainMenu.RunTournament();
    }
    
}

