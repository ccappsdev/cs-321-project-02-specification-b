/**
 *  Match Class: Takes two Fighter Objects and
 *  pits them against one another; handles combat loop.
 *  Creates a Jester object to relay the state of the Match.
 * 
 */
public class Match {

    public Fighter fighter1;
    public Fighter fighter2;
    public Jester jester;

    /**
     *  <p>Match Constructor - creates a new Jester Object</p>
     */
    public Match() {
        this.jester = new Jester();
    }

    /**
     *  <p> Sets the Fighters for the Match</p>
     *  @param f1 Fighter Object taking place in Match (1 of 2)
     *  @param f2 Fighter Object taking place in Match (2 of 2)
     */
    public void setFighters(Fighter f1, Fighter f2) {
        this.fighter1 = f1;
        this.fighter2 = f2;
    }

    /**
     *  <p>Plays through the Match combat loop
     *  Calculates the Attack and Defense performances for each
     *  Fighter, adds stat bonuses, then each Fighter deals damage
     *  one another. First fighter to take ten damage loses.</p>
     *  @return Fighter - winner: Returns the winner of the Match
     *  
     */  
    public Fighter PlayMatch() {
        
        this.jester.CommentOnStart(this.fighter1, this.fighter2);
        
        Boolean isMiddleF1 = false;
        Boolean isMiddleF2 = false;
        Fighter winner = null;
        Fighter loser = null;

        int f1TotalDamageTaken = 0;
        int f2TotalDamageTaken = 0;
        int f1AtkPerformance = 0;
        int f2AtkPerformance = 0;
        int f1DefPerformance = 0;
        int f2DefPerformance = 0;
        int f1DamageTaken = 0;
        int f2DamageTaken = 0;   
        
        System.out.println("\n========================\n" + "NAME: "+ this.fighter1.getName() + "\nReach: " + this.fighter1.getReach() + "\nStrength: "
                + this.fighter1.getStrength() + "\nSpeed: " + 
                this.fighter1.getSpeed() + "\nHealth: "+ (10 - f1TotalDamageTaken) + "\n========================\n" );

        System.out.println("========================\n" + "NAME: "+ this.fighter2.getName() + "\nReach: " + this.fighter2.getReach() + 
                "\nStrength: " + this.fighter2.getStrength() + "\nSpeed: " + 
                this.fighter2.getSpeed() + "\nHealth: "+ (10 - f2TotalDamageTaken) + "\n========================\n" );

        while(winner == null && loser == null) {
            f1AtkPerformance = this.fighter1.GetAttackPerformance();
            f2AtkPerformance = this.fighter2.GetAttackPerformance();
            f1DefPerformance = this.fighter1.GetDefensePerformance();
            f2DefPerformance = this.fighter2.GetDefensePerformance();

            if(this.fighter1.StrongerThan(this.fighter2)) {
                f1AtkPerformance++;
            }
            else if(this.fighter2.StrongerThan(this.fighter1)) {
                f2AtkPerformance++;
            }

            if(this.fighter1.LongerReachedThan(this.fighter2)) {
                f1DefPerformance++;
            }
            else if(this.fighter2.LongerReachedThan(this.fighter1)) {
                f2DefPerformance++;
            }

            if(this.fighter1.FasterThan(this.fighter2)) {
                f1AtkPerformance++;
                f1DefPerformance++;
            }
            else if(this.fighter2.FasterThan(this.fighter1)) {
                f2AtkPerformance++;
                f2DefPerformance++;
            }
 
            if (f2AtkPerformance - f1DefPerformance >= 0)
                f1DamageTaken = f2AtkPerformance - f1DefPerformance;
            if (f1AtkPerformance - f2DefPerformance >= 0)
                f2DamageTaken = f1AtkPerformance - f2DefPerformance;

            f1TotalDamageTaken += f1DamageTaken;
            f2TotalDamageTaken += f2DamageTaken;
            
            System.out.println(this.fighter1.getName() + " takes " + f1DamageTaken + " damage!");
            System.out.println(this.fighter2.getName() + " takes " + f2DamageTaken + " damage!\n");

            System.out.println("\n========================\n" + "NAME: "+ this.fighter1.getName() + "\n" + "Attack Performance: " + 
                    f1AtkPerformance + "\n" + "Defense Performance: " + f1DefPerformance + "\nReach: " + this.fighter1.getReach() + "\nStrength: "
                    + this.fighter1.getStrength() + "\nSpeed: " + 
                    this.fighter1.getSpeed() + "\nHealth: "+ (10 - f1TotalDamageTaken) + "\n========================\n" );

            System.out.println("========================\n" + "NAME: "+ this.fighter2.getName() + "\n" + "Attack Performance: " + 
                    f2AtkPerformance + "\n" + "Defense Performance: " + f2DefPerformance + "\nReach: " + this.fighter2.getReach() + 
                    "\nStrength: " + this.fighter2.getStrength() + "\nSpeed: " + 
                    this.fighter2.getSpeed() + "\nHealth: "+ (10 - f2TotalDamageTaken) + "\n========================\n" );
        

            if(f1TotalDamageTaken >= 5 && !isMiddleF1) {
                this.SignalMiddleToJester(this.fighter1);
                isMiddleF1 = true;
            }
            if(f2TotalDamageTaken >= 5 && !isMiddleF2) {
                this.SignalMiddleToJester(this.fighter2);
                isMiddleF2 = true;
            }

            if(f2TotalDamageTaken >= 10) {
                winner = this.fighter1;
                loser = this.fighter2;
                System.out.println("\n========================\n" + this.fighter1.getName() + " Wins\n" + "========================");
            }
            else if(f1TotalDamageTaken >= 10){
                winner = this.fighter2;
                loser = this.fighter1;
                System.out.println("\n========================\n" + this.fighter2.getName() + " Wins\n" + "========================");
            }

        }
        
        this.jester.CommentOnEnd(winner, loser);

        return winner;
    }

    /**
     *  <p>Signals to the Jester that a Fighter is at half health.</p>
     *  @param fighter Fighter Object the Jester is to report on.
     *  
     */
    private void SignalMiddleToJester(Fighter fighter) {
        this.jester.CommentOnMiddle(fighter);
    }

}

