/***
 * Weapon Class: Holds stats for Weapons created by the WeaponFactory
 * 
 */
public class Weapon {
	
	//Creates archetype,defense, an attack ratings of Weapon
	public WeaponArchetype archetype;
	public int attackRating;
	public int defenseRating;
	
	//Weapons getters an setters
	public void setArchetype(WeaponArchetype archetype) {
		this.archetype = archetype;
	}
	
	public void setAttackRating(int attackRating) {
		this.attackRating = attackRating; 	
	}
	
	public void setDefenseRating(int defenseRating) {
		this.defenseRating = defenseRating; 
	}
	
	public WeaponArchetype getArchetype() {
		return this.archetype;
	}
	
	public int getAttackRating() {
		return this.attackRating;
	}
	
	public int getDefenseRating() {
		return this.defenseRating;
	}

}
