import java.util.ArrayList;
import java.util.Random;


/**
 *  Tournament Class: Composes a list of four SubTournaments.
 *  SubTournament Types: SHORT, MEDIUM, LONG, and WILD
 *  When the Tournament is run, each SubTournament is played out to
 *  determine a winner, then each semifinalist compete in a semifinals
 *  SubTournament, crowning a final winner.
 * 
 */
public class Tournament {
    /**
     * <p>List of SubTournaments to be played</p>
     */
    public ArrayList<SubTournament> subTournaments;

    /**
     *  <p>Tournament Constructor: Initalizes SubTournaments List as well as initializes Tournament.</p>
     * 
     *  @param numMatches number of matches to be fought in each SubTournament
     */
    public Tournament(int numMatches) {

        subTournaments = new ArrayList<SubTournament>();
        InitializeTournament(numMatches);

    }

    /**
     *  <p>Determines the winner of each SubTournament
     *  then pits the semifinalists against each other 
     *  until a final winner is chosen.</p>
     * 
     *  @return Fighter Returns the final winner of the Tournament. 
     * 
     */
    public Fighter DetermineWinner() {
        
        ArrayList<Fighter> semiFinalists = new ArrayList<Fighter>();
        Fighter winner;
        int firstRoundFlag = 0;

        for(SubTournament subTournament : this.subTournaments) {
            System.out.println("#=========================================");
            System.out.println("LET THE " + subTournament.archetype.name() + " SUBTOURNAMENT BEGIN!");
            System.out.println("#=========================================\n\n");
            semiFinalists.add(subTournament.DetermineWinner(firstRoundFlag));
        }

        System.out.println("#=========================================");
        System.out.println("LET THE SEMIFINALS BEGIN!");
        System.out.println("#=========================================");
        SubTournament finalTournament = new SubTournament(TournamentArchetype.WILD, 2, semiFinalists);
        winner = finalTournament.DetermineWinner(firstRoundFlag);

        return winner;

    }

    /**
     *  <p>Sets up the initial conditions for the Tournament.
     *  Creates each SubTournament and populates a List of Fighters
     *  to participate in each SubTournament.</p>
     * 
     *  @param numMatches Number of matches to be fought per SubTournament, used here to determine number of fighters to create.
     * 
     */
    public void InitializeTournament(int numMatches) {
        
        ArrayList<String> namesArray = new ArrayList<String>();
        namesArray.add("Bill");
        namesArray.add("Gunther");
        namesArray.add("Sheila");
        namesArray.add("Rando");
        namesArray.add("Carl");
        namesArray.add("Helga");
        namesArray.add("Nyarlathotep");
        namesArray.add("Bob");
        WeaponFactory weaponFactory = new WeaponFactory();

        ArrayList<Fighter> shortFighters = new ArrayList<Fighter>();
        for (int i = 0; i < (2 * numMatches); i++) {
            Fighter fighter = new Fighter();
            fighter.setName(namesArray.get(new Random().nextInt(namesArray.size())));
            fighter.setStrength(new Random().nextInt(10) + 1); 
            fighter.setReach(new Random().nextInt(10) + 1); 
            fighter.setSpeed(new Random().nextInt(10) + 1);
            fighter.setWeapon(weaponFactory.MakeWeapon(WeaponArchetype.SHORT));
            shortFighters.add(fighter);
        }
        ArrayList<Fighter> mediumFighters = new ArrayList<Fighter>();
        for (int i = 0; i < (2 * numMatches); i++) {
            Fighter fighter = new Fighter();
            fighter.setName(namesArray.get(new Random().nextInt(namesArray.size())));
            fighter.setStrength(new Random().nextInt(10) + 1); 
            fighter.setReach(new Random().nextInt(10) + 1); 
            fighter.setSpeed(new Random().nextInt(10) + 1); 
            fighter.setWeapon(weaponFactory.MakeWeapon(WeaponArchetype.MEDIUM));
            mediumFighters.add(fighter);
        }
        ArrayList<Fighter> longFighters = new ArrayList<Fighter>();
        for (int i = 0; i < (2 * numMatches); i++) {
            Fighter fighter = new Fighter();
            fighter.setName(namesArray.get(new Random().nextInt(namesArray.size())));
            fighter.setStrength(new Random().nextInt(10) + 1); 
            fighter.setReach(new Random().nextInt(10) + 1); 
            fighter.setSpeed(new Random().nextInt(10) + 1); 
            fighter.setWeapon(weaponFactory.MakeWeapon(WeaponArchetype.LONG));
            longFighters.add(fighter);
        }
        ArrayList<Fighter> wildFighters = new ArrayList<Fighter>();
        for (int i = 0; i < (2 * numMatches); i++) {
            Fighter fighter = new Fighter();
            fighter.setName(namesArray.get(new Random().nextInt(namesArray.size())));
            fighter.setStrength(new Random().nextInt(10) + 1); 
            fighter.setReach(new Random().nextInt(10) + 1); 
            fighter.setSpeed(new Random().nextInt(10) + 1); 
            fighter.setWeapon(weaponFactory.MakeWeapon(null));
            wildFighters.add(fighter);
        }

        SubTournament shortTournament = new SubTournament(TournamentArchetype.SHORT, numMatches, shortFighters);
        SubTournament mediumTournament = new SubTournament(TournamentArchetype.MEDIUM, numMatches, mediumFighters);
        SubTournament longTournament = new SubTournament(TournamentArchetype.LONG, numMatches, longFighters);
        SubTournament wildTournament = new SubTournament(TournamentArchetype.WILD, numMatches, wildFighters);

        this.subTournaments.add(shortTournament);
        this.subTournaments.add(mediumTournament);
        this.subTournaments.add(longTournament);
        this.subTournaments.add(wildTournament);

    }

}
