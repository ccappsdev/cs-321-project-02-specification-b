/***
 * WeaponArchetype Enumeration: Possible types of Weapons used by a Fighter.
 */
public enum WeaponArchetype {
	SHORT,
	MEDIUM,
	LONG
}